import { Module, HttpModule } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { LoanManagementModule } from './loan-management/loan-management.module';
import { LoanApplicationController } from './loan-management/controller/loan-application/loan-application.controller';
import { TYPEORM_CONNECTION } from './models/typeorm.connection';
import { ConfigModule } from './config/config.module';
import { ModelsModule } from './models/models.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SetupService } from './system-settings/setup/setup.service';
import { SetupController } from './system-settings/setup/setup.controller';

@Module({
  imports: [
    LoanManagementModule,
    ConfigModule,
    HttpModule,
    ModelsModule,
    TypeOrmModule.forRoot(TYPEORM_CONNECTION),
  ],
  controllers: [
    AppController,
    LoanApplicationController,
    SetupController,
  ],
  providers: [
    AppService,
    SetupService,
  ],
})
export class AppModule {}
