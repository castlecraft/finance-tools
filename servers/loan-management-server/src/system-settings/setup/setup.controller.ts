import { Controller, Post, Body } from '@nestjs/common';
import { SetupService } from './setup.service';
import { SettingsDto } from '../../models/settings/settings.dto';
import { LOAN_MANAGEMENT_SERVER } from '../../constants/app-string';

@Controller('setup')
export class SetupController {
  constructor(private readonly setupService: SetupService) {}

  @Post()
  async setup(@Body() payload: SettingsDto) {
    const settings = Object.assign({}, payload);
    settings.type = LOAN_MANAGEMENT_SERVER;
    return await this.setupService.setup(payload);
  }

}
