export const SEND_EMAIL = 'send_email';
export const AUTHORIZATION = 'authorization';
export const ADMINISTRATOR = 'administrator';
export const TOKEN = 'token';
export const SERVICE_DOWN = 'Connected service is down';
export const LOAN_MANAGEMENT_SERVER = 'loan-management-server';