import { Test, TestingModule } from '@nestjs/testing';
import { LoanApplicationValidationPolicyService } from './loan-application-validation-policy.service';

describe('LoanApplicationValidationPolicyService', () => {
  let service: LoanApplicationValidationPolicyService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [LoanApplicationValidationPolicyService],
    }).compile();

    service = module.get<LoanApplicationValidationPolicyService>(LoanApplicationValidationPolicyService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
