#!/bin/bash

cd /tmp

# Clone building blocks configuration
git clone https://github.com/castlecraft/helm-charts

# Check helm chart is installed or create
# reuse installed values and resets data

export CHECK_AS=$(helm ls -q stg-loan-mgt-server --tiller-namespace finance-tools)
if [ "$CHECK_AS" = "stg-loan-mgt-server" ]
then
    echo "Updating existing stg-loan-mgt-server . . ."
    helm upgrade stg-loan-mgt-server \
        --tiller-namespace finance-tools \
        --namespace finance-tools \
        --reuse-values \
        --recreate-pods \
        helm-charts/loan-management
fi
