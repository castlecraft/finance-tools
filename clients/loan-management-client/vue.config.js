// vue.config.js
module.exports = {
    devServer: {
        proxy: {
            "/info": { "target": "http://localhost:4800", "secure": false, "logLevel": "debug" },
        },
    }
}